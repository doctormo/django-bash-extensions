#!/usr/bin/env python

import os
import sys

TEMP = "/tmp/bash-venv"
PPID = os.environ.get('PPID', None)
PWD = os.environ.get('PWD', None)
if PPID is None or PWD is None:
    sys.stderr.write("Can't find PPID or PWD environ")
    sys.exit(1)

TARGET = os.path.join(TEMP, PPID)
if not os.path.isdir(TEMP):
    os.makedirs(TEMP)

if __name__ == '__main__':
    remove_old_links()

def remove_old_links():
    """Remove any old links"""
    if os.path.islink(TARGET):
        ORIG = os.readlink(TARGET)
        ORIG = os.path.dirname(ORIG)
        if not PWD.startswith(ORIG):
            os.unlink(TARGET)

    else:
        # This is to catch when a new terminal tab is opened and it's in a
        # sub-directory of the last tab. We scan for this parent directory.
        for filename in os.listdir(TEMP):
            path = os.path.join(TEMP, filename)
            if os.path.islink(path):
                ORIG = os.path.dirname(os.readlink(path))
                if PWD.startswith(ORIG):
                    os.chdir(ORIG)
                    break

# TODO - Remove any old symlinks from terminals that have closed

# Identify that this is a pythonenv (used for django)
if [[ -d "pythonenv" ]]; then
    if [[ ! -d $TARGET ]]; then
        # If it is, we make a symbolic link from our temp directory
        ln -s $PWD/pythonenv $TARGET
    fi
fi

# This link's existance is cause for a change in the command line
# and the title for the virtual terminal application.
if [[ -L "$TARGET" ]]; then
     set +e
     ORIG=`readlink $TARGET`
     if [ ! -d "$ORIG" ]; then
         echo "[RM]"
         rm "$TARGET"
         exit 0
     fi
     echo -n "$1"
     cd "$TARGET"
     DIR=`pwd -P`
     DIR=`dirname $DIR`
     cd "$OLDPWD"

     if [ -f "$DIR/*/manage.py" ]; then
         DJ=`ls $DIR/*/manage.py`
         NAME=`dirname $DJ`
     else
         NAME="$PWD"
     fi

     NAME=`basename $NAME`
     c="[base]"

     DIR="$DIR/"
     if [[ "$DIR" != $PWD* ]]; then
         c=${PWD#$DIR}
     fi

     echo -ne "\033]0;$NAME - $c\007"
else
     echo -ne "\033]0;Gnome Terminal$c\007"
fi

